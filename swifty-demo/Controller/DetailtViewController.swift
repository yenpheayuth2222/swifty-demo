//
//  DetailtViewController.swift
//  swifty-demo
//
//  Created by Mavin on 10/11/21.
//

import UIKit

class DetailtViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    var article: Article?
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        print("Title : ",article?.title ?? "")

        titleLabel.text = article?.title
        descriptionLabel.text = article?.description
        let url = URL(string: article!.imageUrl)
        
        let defaultImage = UIImage(systemName: "camera.fill")
        
        self.imageView.kf.setImage(with: url,placeholder: defaultImage, options: [.transition(.fade(0.25))])
        
        // Do any additional setup after loading the view.
    }
    


}
